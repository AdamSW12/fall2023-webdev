/**
 * @description 
 * changeTextColor set the color of HTMLElement tag 
 * with the color value from inputTextColor HTMLElement 
 * @param {HTMLElement} tag 
 * @param {HTMLInputElement} inputTextColor //color input
 */ 
function changeTextColor(tag,inputTextColor){
    tag.style.color = inputTextColor;
}



/**
 * @description 
 * changeTagWidth set the width (in %) of HTMLElement tag 
 * with the value from inputTextWidth HTMLElement 
 * @param {HTMLElement} tag 
 * @param {HTMLInputElement} inputTextWidth //width input
 */
function changeTagWidth(tag, inputTextWidth){
    tag.style.width = inputTextWidth;
}


/**
 * @description 
 * changeBackground set the background of HTMLElement tag 
 * with the value from inputBgdColor HTMLElement 
 * @param {HTMLElement} tag 
 * @param {HTMLInputElement} inputBgdColor //bgd input
 */
function changeBackground(tag, inputBgdColor){
    tag.style.background = inputBgdColor;
}



/**
 * @description 
 * changeBorderColor set the background of a collection of elements tags 
 * with the value from inputBorderColor HTMLElement 
 * @param {HTMLCollection|NodeList} tags 
 * @param {HTMLInputElement} inputBorderColor //border color input
 */
function changeBorderColor(tag, inputBorderColor){
    tag.style.borderColor = inputBorderColor;
}



/**
 * @description 
 * changeBorderWidth set the border width (in px) of a collection of elements tags 
 * with the value from inputBorderWidth HTMLElement 
 * @param {HTMLCollection|NodeList} tags 
 * @param {HTMLInputElement} inputBorderWidth //border width input
 */
function changeBorderWidth(tag, inputBorderWidth){
    tag.style.borderWidth = inputBorderWidth;
}

 /**
 *  @description
 * createMyElement - 
 * . create a dynamic HTMLElement, set its properties
 * . and attach it to the parent (HTMLElement)
 * . The properties (rest parameter- check labs/notes): 
 *      . must have at least id, type,
 *      . optionally text content, value, class name, min, max 
  * 
  * @param {HTMLElement} paremt 
  * @param {HTMLElement} elemTag 
  * @param  {...any} properities 
 * @returns {HTMLElement} / return the created element
  */
    function createMyElement(parent,elemTag,...any){
        let newElem = document.createElement(elemTag);
        if(any[0]){newElem.id = any[0]};
        if(any[1]){newElem.type = any[1]};
        if(any[2]){newElem.className = any[2]};
        if(any[3])(newElem.min = any[3]);
        if(any[4])(newElem.max = any[4]);
        if(any[5])(newElem.value = any[5]);
        if(any[6])(newElem.textContent = any[6]);
        
        parent.appendChild(newElem);
        return newElem;
    }