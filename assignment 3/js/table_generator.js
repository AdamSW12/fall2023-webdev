// must use strict
'use strict';

// step1- 
/**
 * @description -generateTable(rowCount, colCount)
 * generateTable creates a dynamic table with exactly rowCount rows and colCount columns.
 * while loping to create the dynamic cells, you can prepare  the table html code (string) 
 * to append it to section #table-html-space (through a text editor).
 *
 *  Note - Mind the spaces and new lines to format your html output
 * before generating a new table, you should remove an old one if it exists.  
 *
 * advice: you can write this in the console and conduct some tests
 * e.g., generateTable(2,3), generateTable(3, 4)
 * 
 * @param {Number} rowCount //from input nbr-rows
 * @param {Number} colCount //from input nbr-columns
 */
    function generateTable(rowCount,colCount){
        if(document.querySelector("table")){
            let elem = document.querySelector("table");
            elem.parentNode.removeChild(elem);
        }

        //create initial table and its body with new lines for readability in text area
        let table = createMyElement(document.querySelector("#table-render-space"),"table");
        table.innerHTML+="\n<tbody>\n</tbody>\n";

        //generate a row per rowCount and a td per colCount with the current position as text:data(row,column)
        for(let i = 0; i < rowCount; i++){
            table.children[0].innerHTML += "  <tr>\n  ";

            for(let j = 0; j < colCount; j++){
                let text = `data${i+1}${j+1}`;
                table.children[0].children[i].innerHTML += `  <td> ${text} `;
            }
            table.children[0].children[i].innerHTML += "\n  ";
            table.children[0].innerHTML += "\n";
        }

        //delete textarea if there's already one
        if(document.querySelector("textarea")){
            let elem = document.querySelector("textarea");
            elem.parentNode.removeChild(elem);
        }

        //generate a textarea with text content => table html, nulls are unnecessary rest parameters
        createMyElement(document.querySelector("#table-html-space"),"textarea",null,null,null,null,null,table.outerHTML);

    }


/** step2-
 * @description
 * createFormInputTags- 
 * - creates dynamically the SEVEN input HTMLElements (need to call `createMyElement` defined in utilities) 
 *   for each input element:
 *       - set its attributes: id,  type (number or color), default value,
 *       - attach it to a fieldset (with legend), whihc you create on the fly as well.
 *       - register an event listener every time the value in the element has changed (or you can use blur as well)
 * -  for instance:
 *          . row-count Element has to (register) listen to handler `generateTable` defined above.
 *          . col-count Element has to (register) listen to handler `generateTable` defined above.
 *          
 *          . text-color Element has to (register) listen to handler `changeTextColor` defined in utilities
 *          . table-width(%) Element has to (register) listen to handler `changeTagWidth` defined in utilities
 *          . border-width(px) Element has to (register) listen to handler `changeBorderWidth` defined in utilities
 *          ...
*/
function createFormInputTags(){
    let form = document.querySelector("form");
    //create 2 main fieldsets
    createMyElement(form,"fieldset");
    createMyElement(form,"fieldset");

    //create 4 sub-fieldsets that contain legend and input in the first main fieldset
    let mainFieldsets = document.querySelectorAll("form fieldset");
    for(let i = 0; i < 4; i ++){
        //create fieldset with class "inputFieldSet"
        createMyElement(mainFieldsets[0],"fieldset",null,null,"inputFieldset");
    }

    //create 3 sub-fieldsets under the second main fieldset for colours
    for(let i = 0; i < 3; i ++){
        //create fieldset with class "inputFieldSet"
        createMyElement(mainFieldsets[1],"fieldset",null,null,"inputFieldset");
    }

    let subFieldsets = document.querySelectorAll(".inputFieldset");

    //add 7 legends
    let legendText = ["row-count","column-count","table-width(%)","border-width(px)","text-colour","background-colour","(cell)border-colour"];
    for(let i = 0; i < 7; i ++){
        let newLegend = createMyElement(subFieldsets[i],"legend");
        newLegend.innerText = legendText[i];
    }

    //create inputs
    let rows = createMyElement(subFieldsets[0],"input","nbr-rows","number",null,1,10,2);
    let columns = createMyElement(subFieldsets[1],"input","nbr-columns","number",null,1,10,3);
    let tbWidth = createMyElement(subFieldsets[2],"input","table-width","number",null,1,100,100);
    let bdWidth = createMyElement(subFieldsets[3],"input","border-width","number",null,1,10,1);
    let txtClr = createMyElement(subFieldsets[4],"input","text-colour","color");
    let tbBg = createMyElement(subFieldsets[5],"input","table-backgroud","color",null,null,null,"#ffffff");
    let cell = createMyElement(subFieldsets[6],"input","cell-border","color");

    generateTable(rows.value,columns.value);

    //change table columns and rows
    rows.addEventListener("change", function (){
        generateTable(rows.value,columns.value)
    });
    columns.addEventListener("change", function (){
        generateTable(rows.value,columns.value)
    });

    //change the styles depending on inputs
    tbWidth.addEventListener("change",function() {
        changeTagWidth(document.querySelector("table"),tbWidth.value+"%");
    });

    bdWidth.addEventListener("change",function (){
        changeBorderWidth(document.querySelector("table"),bdWidth.value+"px");
    });

    txtClr.addEventListener("change", function(){
        changeTextColor(document.querySelector("table"),txtClr.value);
    });

    tbBg.addEventListener("change",function(){
        changeBackground(document.querySelector("table"),tbBg.value);
    });
    cell.addEventListener("change",function(){
        changeBorderColor(document.querySelector("table"),cell.value);
    });

}


// step 3
// NOTE 
//    - The whole code must run once the HTML code has been parsed (into a DOM tree).
//    - You are not allowed to use defer attribute in the page.html File.
//    - All JS files must be coded inside the <head> section of page.html

// Define a function called  mainProgram as the entry point of the application execution.
// In other words:
//    . is run only when the special event (DOM content) has finished loading
//    .  calls createFormInputTags

//    Note - you can use anonymous function instead of mainProg if you want.

/**
 * @description
 * mainProgram runs when the dom is loaded
 * calls @function createFormInputTags 
 */
function mainProgram(){

    createFormInputTags();
}

document.addEventListener("DOMContentLoaded",mainProgram);