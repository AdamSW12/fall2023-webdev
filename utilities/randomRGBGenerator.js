function randomBetween(minValue, maxValue){
    let ran = Math.random() * (maxValue - minValue) + minValue;
    return Math.floor(ran);
}
function generateColor(){
    let red = randomBetween(0, 255);
    let blue = randomBetween(0,255);
    let green = randomBetween(0,255);
    return `rgb(${red}, ${blue}, ${green})`;
}