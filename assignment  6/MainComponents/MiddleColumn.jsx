const MiddleColumn = (props) =>{
    return(
        <section id = "middle">
            <CommentsList title = "All comments" comments = {props.data}/>
        </section>
    )
}