const Footer = (props) => {
    return (  <footer> {props.name}     {props.copyright} - {new Date().getFullYear()}</footer> );
}