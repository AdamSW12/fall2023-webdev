const CommentsList = (props) =>{
    let title = props.title;
    let[listComments, setListComments] = React.useState(props.comments)

    const deleteComment = (userID)=>{
        setListComments(listComments.filter( (comment)=>comment.id !== userID))
    }

    return(
        <section id="commentList">
            <h2>{title}</h2>
            {
                listComments.map((comment)=>(
                    <section className="comment" key={comment.id}>
                        <section className="ListInfo"> 
                            <section className="ListCmtName">
                            {comment.user}
                            </section>
                            <section className="ListCmtDate">
                                {comment.date}
                            </section>
                        </section>
                        <section className="ListBottom">
                                <section className="ListCmtText">
                                    {comment.text}
                                </section>

                                <section className="ListInteractions">
                                    <section>
                                        <img className="likeImg" src="icons/like.PNG" width="25px"></img>
                                        <p className="ListCmtLikes">{comment.likes}</p>
                                    </section>

                                    <section>
                                        <img className="unlikeImg" src="icons/unlike.PNG" width="25px"></img>
                                        <p className="ListCmtUnlikes">{comment.unlikes}</p>   
                                    </section>
                                    
                                    <section>
                                        <img className="flagImg" src="icons/flag.png" width="25px"></img>
                                        <p className="ListCmtFlag">{comment.flag}</p>
                                    </section>
                                    
                                    <section>
                                        <img className="delImg" src="icons/delete.png" width="25px"></img>
                                        <button onClick={()=>deleteComment(comment.id)}>del</button>
                                    </section>
                                </section>
                        </section>
                    </section>
                ))
            }
        </section>
    )
}