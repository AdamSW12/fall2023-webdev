const LeftColumn = (props) =>{
    const sortData = (sortType) => {
        let newList
        if(sortType === 0){
            newList = props.data.sort((c1,c2)=> (c1.likes - c2.likes))
        }
        else{
            newList = props.data.sort((c1,c2)=> (c2.likes - c1.likes))
        }
        newList = newList.slice(0,5)
        console.log(newList);
        return newList
    }

    let mostLiked
    let leastLiked
    {leastLiked = sortData(0)}
    {mostLiked = sortData(1)}
    return(
        <section id = "left">
            <h3>some stats</h3>
            <p id = "topCmtMsg">top 5 liked comments</p>
            <table id = "mostLikedComments">
                <thead>
                    <tr>
                        <th>user</th>
                        <th>nbrComments</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        mostLiked.map((comment)=>(
                            <tr key = {comment.id}>
                                <td className="userNames"> {comment.user}</td>
                                <td className="likes"> {comment.likes}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            <p id = "bottomCmtMsg">top 5 unliked comments</p>
            <table id = "leastLikedComments">
                <thead>
                    <tr>
                        <th>user</th>
                        <th>nbrComments</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        leastLiked.map((comment)=>(
                            <tr key = {comment.id}>
                                <td className="userNames"> {comment.user}</td>
                                <td className="dislikes"> {comment.unlikes}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>

        </section>
    )
}

