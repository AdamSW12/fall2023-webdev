const RightColumn = (props) =>{

    const filterFlags = () =>{
        let newList = props.data.filter((comment)=> comment.flag !== 0)
        return newList
    }
    let flagged
    {flagged = filterFlags()}
    console.log(flagged)

    return(
        <section id="right">
            <h3>some stats</h3> 
            <p id="flaggedMsg">flagged comments</p>
            <table>
                <thead>
                    <tr>
                        <th>user</th>
                        <th>likes</th>
                        <th>dislikes</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        flagged.map((comment)=>(
                            <tr key = {comment.id}>
                                <td className="userNames">{comment.user}</td>
                                <td className="likes"> {comment.likes}</td>
                                <td className="dislikes"> {comment.likes}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </section>
    )
}