const Header = (props) => {
    return ( 
    <section id ="head">
        <header><h1> {props.title}</h1> </header> 
        <Nav /> 
    </section>);
}