const Main = () => {
    let comments = commentsData()
    return ( <section id="main">
        <LeftColumn data={comments}/>
        <MiddleColumn data = {comments}/>
        <RightColumn data={comments}/>
    </section> );
}
