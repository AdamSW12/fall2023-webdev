const App = () => {
    return ( 
        <>
        <Header title="Assignment 3" />
        <Main />
        <Footer name="Adam Winter" copyright="copy right "/>
        </>
     );
}
ReactDOM.render( <App/>, document.querySelector("#root"))